<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.12.15
 * Time: 20:49
 */

require_once('functions.php');
require_once('header.php');
if(!isUserLoggedIn()){
    header("Location: index.php");

}

if(isset($_POST['delete']))
{
    deleteTopicById($link, $_GET['id']);

    header('Location: index.php');


} elseif (isset($_POST['cancel']))
{
    header('Location: index.php');
}
?>

<p>Вы действительно хотите удалить запись?</p>
<form action="" method="post">
    <input type="submit" value="Удалить" name="delete">
    <input type="submit" value="Отмена" name="cancel">
</form>
