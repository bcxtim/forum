/**
 * Created by root on 12.12.15.
 */
$(document).ready(function() {

  $(document).on("submit", "#loginform", function() {
      $(".error").remove();
      var login = $("#login").val().length;
      if(login < 3) {
          $("#login").after("<div class='error'> Минимальный логин 3 символа </div>");

          return false;
      }

  });

    /**
     * Проверка на количество символов для добавления в комментарий
     */
    $("#comment").keyup(function() {
        $(".char-count-comment>i").empty();
       var comment = $("#comment").val().length;
        var сountComment = 50 - comment;
        $("<span>"+ сountComment + "</span>").appendTo(".char-count-comment>i");
        if(сountComment == 0) {
            $("#comment").css("border-color", "green");
        }


    });

    /**
     * Если число символов оставшихся для ввода в комментарии, не равно нулю, то не давать сабмитить форму
     */
    $(document).on("submit", "#addComment", function() {
        var comment = $("#comment").val().length;
        var сountComment = 50 - comment;
      if (сountComment !=0)
      {
          return false;
      }
    });


});
