<?php


require_once('functions.php');
require_once('header.php');
require_once('topics.php');

$user_auth = isUserLoggedIn();

if($user_auth)
{
    echo "Привет ".$_SESSION['user_login'];
}

$lastTopics = selectLast4Topics($link);
$lastComments = selectLast4Comments($link);



?>

<h1>Все темы форума</h1>

<?php if($user_auth): ?>
<form action="add-topic.php" method="post">
    <input type="submit" value="Добавить топик">
</form>
<?php endif; ?>

<div class="row">
    <div class="col-md-9">
        <div class="forum-name">Тестирование</div>
        <div class="background-forum">
            <div class="background-table">
                <table class="table table-hover">
                    <?php foreach($topics as $topic): ?>

                        <?php  // узнаем количество комментов в топике
                        $count = countIdCommentByIdTopic($link, $topic['id']);

                        ?>
                    <tr>
                        <td><span class="glyphicon glyphicon-pencil"></span></td>
                        <td><a href="topic.php?id=<?php echo $topic['id']?>"> <?= $topic['title']; ?></a></td>
                        <td><b><?= $count; ?></b> комментов </td>
                        <td>Автор: <?= $topic['user_login']; ?></td>
                        <?php if($user_auth): ?>
                            <td><a href="delete-topic.php?id=<?php echo $topic['id']; ?>">Удалить топик</a> </td>
                        <?php endif; ?>
                    </tr>
                    <?php endforeach; ?>

                </table>
            </div>
        </div>
        <br>
        <div class="forum-name">Тестирование</div>
        <div class="background-forum">
            <div class="background-table">
                <table class="table table-hover">
                    <?php foreach($topics as $topic): ?>

                        <?php  // узнаем количество комментов в топике
                        $count = countIdCommentByIdTopic($link, $topic['id']);

                        ?>
                        <tr>
                            <td><span class="glyphicon glyphicon-pencil"></span></td>
                            <td><a href="topic.php?id=<?php echo $topic['id']?>"> <?= $topic['title']; ?></a></td>
                            <td><b><?= $count; ?></b> комментов </td>
                            <td>Автор: <?= $topic['user_login']; ?></td>
                            <?php if($user_auth): ?>
                                <td><a href="delete-topic.php?id=<?php echo $topic['id']; ?>">Удалить топик</a> </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>

                </table>
            </div>
        </div>



    </div>
    <div class="col-md-3">
        <div class="forum-name">Последние темы</div>
        <div class="background-sidebar">
            <?php foreach($lastTopics as $lastTopic): ?>
            <p>
                <a href="topic.php?id=<?= $lastTopic['id']; ?>"> <?= $lastTopic['title']; ?></a> <br>
                <?= $lastTopic['user_login']; ?> <?= $lastTopic['date']; ?>
            </p>
            <?php endforeach; ?>
        </div>
        <br />
        <div class="forum-name">Последние комментарии</div>
        <div class="background-sidebar">
            <?php foreach($lastComments as $lastComment): ?>
            <p>
                <a href="topic.php?id=<?= $lastComment['id_topic']; ?>"> <?= $lastComment['title']; ?></a> <br>
                <?= $lastComment['user_login']; ?> <br />
                <?= $lastComment['date']; ?>
            </p>
            <?php endforeach; ?>

        </div>
    </div>
</div>



<?php require_once('footer.php'); ?>


