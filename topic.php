<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.12.15
 * Time: 17:01
 */



require_once('functions.php');
require_once('header.php');
$user_auth = isUserLoggedIn();

$topic = getTopicById($link, $_GET['id']);



if(isset($_POST['send-comment']))
{
    $user_id = $_SESSION['id'];
    $text = mysqli_real_escape_string($link, $_POST['comment']);
    $result = addCommentByIdTopic($link, $_GET['id'], $text, $user_id);
    header('Location: topic.php?id='.$topic['id']);

}
$comments = getCommentsByIDTopic($link, $_GET['id']);




?>

<table>
    <tr>
        <th>Автор</th>
        <th>Название темы</th>
        <th>Описание темы</th>
        <th>Дата публикации</th>
    </tr>
    <tr>
        <td><?= $topic['user_login']; ?></td>
        <td><?= $topic['title']; ?></td>
        <td><?= $topic['description']; ?></td>
        <td><?= $topic['date']; ?></td>
    </tr>
</table>

<p>Таблица комментов</p>

<table>
    <tr>
        <th>Автор коммента</th>
        <th>Текст коммента</th>
        <th>Дата публикации</th>
        <?php if($user_auth): ?>
        <th>Удаление коммента</th>
        <?php endif; ?>
    </tr>
    <?php foreach($comments as $comment): ?>
    <tr>
        <td><?= $comment['user_login']; ?></td>
        <td><?= $comment['text']; ?></td>
        <td><?= $comment['date']; ?></td>
        <?php if($user_auth): ?>
        <td><a href="delete-comment.php?id=<?= $comment['id']?>">Удалить коммент</a> </td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>
</table>

<br>
<form action="" method="post">
<input type="submit" value="Добавить комментарий" name="add-comment">
</form>

<!--если нажата кнопка добавить коммент, то открываем форму-->

<?php if(isset($_POST['add-comment'])):   ?>
    <?php if(isUserLoggedIn()): ?>

<br><br><br>
<form action="" method="post" id="addComment">
    <p>Комментарий</p>
    <textarea name="comment" rows="20" cols="70" id="comment" maxlength="50"></textarea>
    <br><br>
    <input type="submit" value="Отправить комментарий" name="send-comment" id="com-submit" >
    <span class="char-count-comment">Необходимо ввести еще <i>50</i> символов</span>
</form>
        <?php else: header("Location: login.php?redirectURL=topic.php?id=".$_GET['id']); ?>

<?php endif; ?>
<?php endif; ?>
