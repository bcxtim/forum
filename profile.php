<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.12.15
 * Time: 20:23
 */
require_once('functions.php');
session_start();
if(!isUserLoggedIn())
{
    header("Location: index.php");
}
require_once('header.php');

if(isset($_POST['change_email']))
{
    $err = array(); // массив с ошибками

    $currentEmail = mysqli_real_escape_string($link, $_POST['current_email']);
    $newEmail = mysqli_real_escape_string($link, $_POST['new_email']);
    $password = mysqli_real_escape_string($link, md5(md5(trim($_POST['password']))));
    $email_db = checkEmailByUserId($link, $_SESSION['id']);

    //проверка, принадлежит ли е-маил данному пользователю
    if($email_db != $currentEmail) {
        $err[] = 'Вы ввели неправильный е-маил';
    }
    //проверка, существует ли мыло у другого пользователя
    $user_email = checkEmailInDb($link, $newEmail);
    $emailCount = reset(mysqli_fetch_assoc($user_email));
    if($emailCount > 0) // если результат больше 0 то ошибка
    {
        $err[] = 'Пользователь с таким е-маилом уже существует';
    }

    // проверяем, совпадает ли пароль с данного пользователя
    if($password != checkPasswordByUserId($link, $_SESSION['id']))
    {
        $err[] = 'Введенный Вами пароль не совпадает с действительным';
    }

    // проверяем, если нет ошибок, то добавляем меняем е-маил, если есть, то выводим ошибки
    if(count($err) == 0)
    {
        changeEmailByIdUser($link, $_SESSION['id'], $newEmail);

        echo "Е-маил успешно сменен";
    } else
    {
        foreach($err as $error)
        {
            echo $error."<br>";
        }

    }

}

?>

<h2 >Сменить е-маил</h2>
<form method="post" action="" id="change-email">
    <p>Введите Ваш действующий е-маил</p>
    <input type="email" name="current_email"> <br />
    <p>Введине Ваш новый е-маил</p>
    <input type="email" name="new_email"> <br />
    <p>Для подтверждения смены е-маила, введите Ваш действующий пароль</p>
    <input type="password" name="password"> <br />
    <p></p>
    <input type="submit" name="change_email" value="Сменить е-маил">

</form>
