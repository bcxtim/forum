<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19.12.15
 * Time: 20:05
 */
session_start();
require_once('functions.php');
if(!isUserLoggedIn())
{
    header("Location: index.php");
}

require_once('header.php');

$users = selectAllUsers($link);

?>

<table class="users_table" border="1">
    <tr>
        <th>Логин пользователя</th>
        <th>Е-маил пользователя</th>
        <th>Дата регистрации</th>
    </tr>
    <?php foreach($users as $user): ?>
    <tr>
        <td><a href="user.php?id=<?= $user['id']; ?>"> <?= $user['user_login']; ?> </a></td>
        <td><?= $user['user_email']; ?></td>
        <td><?= $user['user_registration']; ?></td>
    </tr>
    <?php endforeach; ?>
</table>
