<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.12.15
 * Time: 19:34
 */
require_once('functions.php');
session_start();


if(isUserLoggedIn())
{

    header('Location: index.php');
}

require_once('header.php');

/**
 * Авторизуем пользователя, если логин и пароль есть в базе
 */
if(isset($_POST['auth']))
{
   $user_login =  mysqli_real_escape_string($link,$_POST['login']);
   $user_password = mysqli_real_escape_string($link, md5(md5($_POST['password'])));

    $result = loginUser($link, $user_login, $user_password); // получаем результат запроса
    $user_data = mysqli_fetch_assoc($result); // прогоняем результат через массив

    //проверяем, есть ли в базе пользователь с логином и паролем

    if($user_data['user_login'] == $user_login && $user_data['user_password'] == $user_password)
    {
        $_SESSION['id'] = $user_data['id'];
        $_SESSION['user_login'] = $user_data['user_login'];
        //проверяем, если пользователь пришел с какой-то страницы с редиректом
        if(!empty($_GET['redirectURL']))
        {
            header("Location: ".$_GET['redirectURL']); //то возвращем его на эту страницу
        } else // если с обычной страницы без редиректа
            header('Location: index.php'); // то возвращем его на главную
    } else
    {
        echo "Такого пользователя не существует";
    }

}

?>

<form action="" method="post" id="loginform">
   Логин  <input  type="text" name="login"  id="login"> <br />
    <br />
   Пароль <input  type="password" name="password"> <br />
    <br />
    <input type="submit" name="auth" value="Войти" id="auth">
</form>
