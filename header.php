<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.12.15
 * Time: 16:30
 */
session_start();
header("Content-type: text/html; charset=utf-8");


?>

<DOCTYPE html>
    <html>
        <head>
            <title>Форум</title>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <script type="text/javascript" src="js/jquery-2.1.4.js"></script>
            <script type="text/javascript" src="js/jquery.validate.js"></script>
            <script type="text/javascript" src="js/script.js"></script>
            <link href="css/style.css" rel="stylesheet">

        </head>
    <body>
    <!--Начало шапки-->
    <div class="container-fluid" id="header">
        <div class="row">
            <div class="col-md-8">
                <header>
                    <a href="index.php">Мой форум</a>
                </header>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="panel-group text-right">
                    <?php if(isset($_SESSION['id'])): ?>
                        <a href="profile.php">Мой профиль</a>
                        <a href="users.php">Вcе пользователи</a>
                        <a href="logout.php">Выйти</a>
                    <?php else: ?>
                        <a href="login.php">Вход</a>или
                        <a href="registration.php">Зарегистрироваться</a>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <!--Конец шапки-->
    <div class="container">
        <div class="row">
            <h1>Название сайта</h1>
            <!--Навигация-->
            <div class="navbar navbar-default"> <!--Темная или светлая навигация-->
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responsive-menu">
                            <span class="sr-only">Открыть навигацию</span>
                            <span class="icon-bar"></span> <!--сколько линий в ко-->
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!--<a class="navbar-brand" href="#">Название компании</a>-->
                    </div>
                    <div class="collapse navbar-collapse" id="responsive-menu">
                        <ul class="nav navbar-nav">
                            <li><a href="#">Форум</a> </li>
                            <li><a href="#">Обо мне</a> </li>
                            <li><a href="#">Контакты</a> </li>
                        </ul>
                        <form class="navbar-form pull-right">

                            <input type="text" class="form-control input-md "  placeholder="Найти..." >

                        </form>
                    </div>
                </div>
            </div>
        </div>






