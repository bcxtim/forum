<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19.12.15
 * Time: 21:04
 */
session_start();
require_once('functions.php');
if(!isUserLoggedIn())
{
    header("Location: index.php");
}
require_once('header.php');

$user = getUserById($link, $_GET['id']);

?>

<h2>Данные пользователя</h2>
<table border="1" class="users_table" >
    <tr>
        <th>ID пользователя</th>
        <th>Имя пользователя</th>
        <th>Е-маил</th>
        <th>Пароль</th>
        <th>Дата регистрации</th>
    </tr>
    <tr>
        <td><?= $user['id']; ?></td>
        <td><?= $user['user_login']; ?></td>
        <td><?= $user['user_email']; ?></td>
        <td><?= $user['user_password']; ?></td>
        <td><?= $user['user_registration']; ?></td>

    </tr>
</table>
