<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08.12.15
 * Time: 20:37
 */

require_once('functions.php');

//если пользователь авторизован, бросаем его на главную
if(isUserLoggedIn())
{
    header("Location: index.php");
}

require_once('header.php');

/**
 * Регистрация пользователя
 */

if(isset($_POST['reg']))
{
    $err = array();
    $login = mysqli_real_escape_string($link,$_POST['login']);
    $password = mysqli_real_escape_string($link, md5(md5(trim($_POST['password']))));
    $email = mysqli_real_escape_string($link, $_POST['email']);

    //проверяем логин


    if(!preg_match("/^[a-zA-Z0-9]+$/", $login))
    {
        $err[] = "Логин может состоять из английских букв и цифр";
    }

    if(strlen($login) < 3 or strlen($login) > 10)
    {
        $err[] = "Логин не может быть меньше 3 и больше 10 символов";
    }

    //проверка, есть ли в базе логин
    $user_login = checkLoginInDb($link, $login); // делаем запрос к базе
    $loginCountArray = mysqli_fetch_assoc($user_login); // результат загоняем в массив
    $loginCount = reset($loginCountArray); // вытаскиваем значение COUNT id

    if($loginCount > 0) // проверяем, чтобы количество логинов не было больше нуля
    {
        $err[] = "Такой логин уже есть в базе";
    }

    //проверяем на валидность е-маил
    if(!preg_match("/^[a-zA-Z0-9]+@[a-zA-Z0-9]+[.a-zA-Z0-9]+$/", $email))
    {
        $err[] = "Пожалуйста, укажите валидный е-маил";
    }

    $user_email = checkEmailInDb($link, $email); // делаем запрос на проверку е-мала
    $emailCountArray = mysqli_fetch_assoc($user_email); // результат загоняем в массив
    $emailCount = reset($emailCountArray); // получаем результат COUNT

    if($emailCount > 0)
    {
        $err[] = "Такой е-маил уже есть в базе";
    }

    //проверяем на отсутствие ошибок
    if(count($err) == 0)
    {

        $result = addNewUser($link, $login, $password, $email); // добавляем юзера и возвращаем айди созданной записи
        $user = selectUserById($link, $result); // вытаскиваем данные юзера по айди

        $_SESSION['id'] = $user['id'];
        $_SESSION['user_login'] = $user['user_login'];

        header('Location: index.php');

    } else
    {
        echo "При регистрации возникли следующие ошибки "."<br>";
        foreach($err as $error)
        {
            echo $error."<br>";
        }

    }
}

?>


<form method="post" action="" id="regform">
    Логин <input type="text" name="login"> <br><br>
    Пароль <input type="password" name="password"> <br><br>
    E-mail <input type="email" name="email"> <br><br>
    <input type="submit" name="reg" value="Зарегистрироваться">
</form>
