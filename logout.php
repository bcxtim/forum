<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09.12.15
 * Time: 19:34
 */
session_start();

unset($_SESSION['id']);
unset($_SESSION['user_login']);

header('Location: index.php');