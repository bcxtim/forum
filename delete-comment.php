<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07.12.15
 * Time: 20:55
 */
require_once('functions.php');
require_once('header.php');

if(!isUserLoggedIn())
{
    header("Location: index.php");
}

$comment = getCommentById($link, $_GET['id']);
$id_topic = $comment['id_topic'];


deleteCommentById($link, $_GET['id']);
header('Location: topic.php?id='.$id_topic);