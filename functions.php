<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.12.15
 * Time: 16:19
 */


require_once('db.php');

/**
 * Выгрузить все топики
 * @param $link
 * @return array
 */

function getAllTopics($link)
{
    $query = "SELECT t.*, u.user_login FROM topic AS t LEFT JOIN users AS u ON t.id_user = u.id";
    $result = mysqli_query($link, $query) or die("Невозможно выгузить все топики".mysqli_error($link));
    $topics = fetchArrayAll($result);
    return $topics;
}

/**
 * Загнать в ассоциативный массив результаты
 * @param $result
 * @return array
 */

function fetchArrayAll($result)
{
    $rows = array();
    while($row = mysqli_fetch_assoc($result))
    {
        $rows[$row['id']] = $row;
    }
    return $rows;


}

/**
 * Загнать в один результат в ассоциативный массив
 * @param $result
 * @return array|null
 */
function fetchArrayOne($result)
{
    $row = mysqli_fetch_assoc($result);
    return $row;
}

/**
 * Выгрузить топик по id
 * @param $link
 * @param $id
 * @return array
 */
function getTopicById($link, $id)
{
    $topic_id = intval($id);
    $query = "SELECT t.*, u.user_login FROM topic AS t LEFT JOIN users AS u  ON t.id_user = u.id WHERE t.id = '$topic_id'";

    $result = mysqli_query($link, $query) or die("Невозмоно выгрузить данные поста".mysqli_error($link));
    $topic = fetchArrayOne($result);

    return $topic;

}

/**
 * Вытащить коммент по айди
 * @param $link
 * @param $id
 * @return array|null
 */

function getCommentById($link, $id)
{
    $comment_id = intval($id);
    $query = "SELECT * FROM comment WHERE id = '$comment_id'";

    $result = mysqli_query($link, $query) or die("Невозмоно выгрузить данные поста".mysqli_error($link));
    $topic = fetchArrayOne($result);

    return $topic;
}

/**
 * Добавить коммент в айди топика
 * @param $link
 * @param $id_topic
 * @param $user_id
 * @param $text
 * @return bool
 */
function addCommentByIdTopic($link, $id_topic, $text, $user_id)
{

    $query = "INSERT INTO comment (id_topic, text, id_user) VALUES ('$id_topic', '$text', '$user_id') ";
    $result = mysqli_query($link, $query) or die("Не добавился коммент".mysqli_error($link));
    return true;
}

/**
 * Вытащить комменты по айди топика
 * @param $link
 * @param $id
 * @return array
 */

function getCommentsByIDTopic($link, $id)
{
    $id_topic = intval($id);
    $query = "SELECT c.*, u.user_login FROM comment AS c LEFT JOIN users AS u ON c.id_user = u.id WHERE id_topic = '$id_topic'";
    $result = mysqli_query($link, $query) or die("Невозможно достать комменты".mysqli_error($link));
    $comments = fetchArrayAll($result);
    return $comments;
}

/**
 * Удалить топик по айди
 * @param $link
 * @param $id
 * @return bool
 */

function deleteTopicById($link, $id)
{
    $id_topic = intval($id);
    $query = "DELETE FROM topic WHERE id = '$id_topic'";
    $result = mysqli_query($link, $query) or die("Невозможно удалить топик".mysqli_error($link));
    return true;

}

/**
 * Добавление топика от имени юзера
 * @param $link
 * @param $title
 * @param $description
 * @param $id_user
 * @return bool
 */
function addTopic($link, $title, $description, $id_user)
{
    $query = "INSERT INTO topic (title, description, id_user) VALUES ('$title', '$description', $id_user)";
    $result = mysqli_query($link, $query) or die("Невозможно добавить топик".mysqli_error($link));
    return true;
}

/**
 * Удаление коммента по id
 * @param $link
 * @param $id
 */

function deleteCommentById($link, $id)
{
    $id_comment = intval($id);
    $query = "DELETE FROM comment WHERE id = '$id_comment'";
    $result = mysqli_query($link, $query) or die("Невозможно удалить коммент".mysqli_error($link));

}

/** Проверка есть ли такой логин уже в БД
 * @param $link
 * @param $user_login
 * @return bool|mysqli_result
 */
function checkLoginInDb($link, $user_login)
{
    $query = "SELECT COUNT(id) FROM users WHERE user_login = '$user_login'";

    $result = mysqli_query($link, $query) or die("Произошла ошибка при попытке узнать пользователя с айди".mysqli_error($link));
    return $result;

}

/**
 * Проверка, есть ли такой же е-маил в БД
 * @param $link
 * @param $user_email
 * @return bool|mysqli_result
 */

function checkEmailInDb($link, $user_email)
{
    $query = "SELECT COUNT(id) FROM users WHERE user_email = '$user_email'";
    $result = mysqli_query($link, $query) or die("Произошла ошибка при попытке узнать пользователя с айди".mysqli_error($link));
    return $result;
}

/**
 * Добавляем нового юзера
 * @param $link
 * @param $user_login
 * @param $user_password
 * @param $user_email
 * @return bool
 */
function addNewUser($link, $user_login, $user_password, $user_email)
{
    $query = "INSERT INTO users (user_login, user_password, user_email) VALUES ('$user_login', '$user_password', '$user_email')";
    $result = mysqli_query($link, $query) or die("Невозможно зарегать юзера".mysqli_error($link));
    $user_id = mysqli_insert_id($link);
    return $user_id;

}

/**
 * Авторизация юзера если есть юзер в БД
 * @param $link
 * @param $user_login
 * @param $user_password
 * @return bool|mysqli_result
 */

function loginUser($link, $user_login, $user_password)
{
    $query = "SELECT * FROM users WHERE user_login = '$user_login' AND user_password = '$user_password'";
    $result = mysqli_query($link, $query) or die("Невозможно узнать пользователя".mysqli_error($link));
    return $result;
}

/**
 * Выбираем юзера по айди
 * @param $link
 * @param $id
 * @return array|null
 */

function selectUserById($link, $id)
{
    $user_id = intval($id);
    $query = "SELECT * FROM users WHERE id = '$user_id'";
    $result = mysqli_query($link, $query) or die("Невозможно вытащить юзера".mysqli_error($link));
    $user = fetchArrayOne($result);
    return $user;
}

/**
 * Проверяем, залогинен юзер или нет. Если залогинен, то true, если нет - то false
 * @return bool
 */
function isUserLoggedIn()
{
    if(isset($_SESSION['id']))
    {
        return true;
    } else
    {
        return false;
    }

}

/**
 * Проверяем, принадлежит ли пользователю данный е-маил
 * @param $link
 * @param $id
 * @return array|null
 */

function checkEmailByUserId($link, $id)
{
    $user_id = intval($id);
    $query = "SELECT user_email FROM users WHERE id = '$user_id'";
    $result = mysqli_query($link, $query) or die("Невозможно распознать е-маил".mysqli_error($link));
    $user_email = reset(fetchArrayOne($result));
    return $user_email;

}

/**
 * узнать пароль по айди
 * @param $link
 * @param $id
 * @return mixed
 */

function checkPasswordByUserId($link, $id)
{
    $user_id = intval($id);
    $query = "SELECT user_password FROM users WHERE id = '$user_id'";
    $result = mysqli_query($link, $query) or die("Невозможно распознать е-маил".mysqli_error($link));
    $user_password = reset(fetchArrayOne($result));
    return $user_password;
}

/**
 * Сменить е-маил у юзера
 * @param $link
 * @param $id
 * @param $user_email
 */
function changeEmailByIdUser($link, $id, $user_email)
{
    $user_id = intval($id);
    $query = "UPDATE users SET user_email = '$user_email' WHERE id = '$user_id' ";
    $result = mysqli_query($link, $query) or die("Невозможно сменить е-маил".mysqli_error($link));

}

/**
 * Отобразить все данные всех пользователей
 * @param $link
 * @return array
 */

function selectAllUsers($link) {
    $query = "SELECT * FROM users";
    $result = mysqli_query($link, $query) or die("Невозможно вытащить юзеров".mysqli_error($link));
    $users = fetchArrayAll($result);

    return $users;
}

/**
 * Вытащить данные юзера по айди юзера
 * @param $link
 * @param $id
 * @return array|null
 */

function getUserById($link, $id)
{
    $user_id = intval($id);
    $query = "SELECT * FROM users WHERE id = '$user_id'";
    $result = mysqli_query($link, $query) or die("Невозможно вытащить юзера".mysqli_error($link));
    $user = fetchArrayOne($result);
    return $user;
}

/**
 * Узнаем количество комментов в топике
 * @param $link
 * @param $id
 * @return bool|mysqli_result
 */

function countIdCommentByIdTopic($link, $id)
{
    $id_topic = intval($id);
    $query = "SELECT COUNT(id) FROM comment WHERE id_topic = '$id_topic'";
    $result = mysqli_query($link, $query) or die("Невозможно узнать количество комментов".mysqli_error($link));
    $countComment = fetchArrayOne($result);
    $count = reset($countComment);
   return $count;
}

/**
 * Выбрать последние четыре топика
 * @param $link
 * @return array
 */
function selectLast4Topics($link) {
    $query = "SELECT t.*, u.user_login FROM topic  AS t LEFT JOIN  users AS u ON u.id = t.id_user ORDER BY id DESC LIMIT 4";
    $result = mysqli_query($link, $query) or die("Невозможно вытащить последние 4 записи".mysqli_error($link));
    $topics = fetchArrayAll($result);
    return $topics;
}

/**
 * отобразить последние четыре коммента
 * @param $link
 * @return array
 */

function selectLast4Comments($link)
{
    $query = "SELECT c.*, t.title, u.user_login FROM comment  AS c LEFT JOIN  users AS u ON u.id = c.id_user LEFT JOIN topic AS t  ON u.id = t.id_user ORDER BY id DESC LIMIT 4";
    $result = mysqli_query($link, $query) or die("Невозможно вытащить последние 4 записи".mysqli_error($link));
    $comments = fetchArrayAll($result);
    return $comments;
}


