<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06.12.15
 * Time: 21:39
 */
require_once('functions.php');
require_once('header.php');
if(!isUserLoggedIn())
{
    header("Location: index.php");
}

if(isset($_POST['add-topic']))
{
    $user_id = $_SESSION['id'];
    $title = mysqli_real_escape_string($link, $_POST['title']);
    $description = mysqli_real_escape_string($link, $_POST['description']);
    addTopic($link, $title, $description, $user_id);
    header('Location: index.php');

}

?>

<form action="" method="post" id="addTopic">
    <p>Название темы</p>
    <input type="text" name="title">
    <p>Текст</p>
    <textarea name="description" rows="20" cols="70"> </textarea>
    <p></p>
    <input type="submit" name="add-topic" value="Добавить топик">
</form>
